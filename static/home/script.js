$(document).ready(function(){
    $(".toggle-dark").click(toDark);
    $(".accordion").accordion({
        collapsible: true,
        active: false
    });
});

function toDark() {
    $("body").css("background-color", "#555555");
    $(".alghi-text").css("color", "#eeeeee");
    $(".ui-accordion-content").css("background-color", "rgb(110, 110, 110)");
    $(".ui-accordion-content").css("color", "white");
    $(".ui-accordion-content").css("border", "none");
    $(".toggle-dark").click(toLight);
}

function toLight() {
    $("body").css("background-color", "white");
    $(".ui-accordion-content").css("background-color", "white");
    $(".ui-accordion-content").css("border", "1px solid #dddddd");
    $(".ui-accordion-content").css("color", "black");
    $(".alghi-text").css("color", "black");
    $(".toggle-dark").click(toDark);
}