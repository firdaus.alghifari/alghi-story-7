from django.test import TestCase, Client
from django.urls import resolve

from .views import index

class IndexTest(TestCase):
    def test_index_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home/index.html')


# SELENIUM TEST #
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

class IndexLiveTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        self.selenium = webdriver.Chrome(chrome_options = chrome_options)
        super(IndexLiveTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(IndexLiveTest, self).tearDown()

    def test_check_title(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)
        self.assertEqual(selenium.title, 'Alghi Story 7')

    def test_check_content(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)
        self.assertIn('Firdaus Al-Ghifari', selenium.page_source)
        
    def test_css_background(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)
        body = selenium.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(255, 255, 255, 1)')

    def test_dark_theme(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)
        dropdown = selenium.find_element_by_css_selector('.toggle-dark')
        dropdown.send_keys(Keys.RETURN)
        time.sleep(5)

        body = selenium.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')

        self.assertEqual(background_color, 'rgba(85, 85, 85, 1)')
